{
  description = "Helper file for a custom os";
  outputs = { self, nixpkgs }:
  let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
      config = { allowUnfree = true; };
    };
    selfPkgs = self.packages.x86_64-linux;
  in {
    apps.x86_64-linux.default = {
      type = "app";
      program = "${selfPkgs.run}/bin/run";
    };

    packages.x86_64-linux = import ./pkgs {
      callPackage = pkgs.callPackage;
      lib = pkgs.lib;
      pkgs = pkgs;
      selfPkg = self.packages.x86_64-linux;
    };

    devShells.x86_64-linux.default = pkgs.mkShell {
      packages = with pkgs; [
        git
	neovim
	nasm
	bvi
	file
      ];
    };
  };
}
