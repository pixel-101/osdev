{ lib
, stdenv
, nasm
}:

stdenv.mkDerivation (finalAttrs: {
  name = "os-bios";
  src = ../../src/bios;

  buildInputs = [
    nasm
  ];

  buildPhase = ''
    nasm boot.asm -f bin -o boot.bin
  '';
  installPhase = ''
    mkdir -p $out/share
    mv boot.bin $out/share
  '';

  meta = with lib; {
    description = "VM Bios";
    licence = licences.bsd3;
    platforms = platforms.linux;
  };
})
