{ lib
, stdenv
, qemu
, dash
, all
}:

stdenv.mkDerivation (finalAttrs: {
  name = "vm-runner";
  src = ./.;

  buildInputs = [
    qemu
    dash
    all
  ];

  buildPhase = ''
    cat <<EOF > run
  #!${dash}/bin/dash
  cp ${all}/share/full.img ./disk.img
  chmod 644 ./disk.img
  ${qemu}/bin/qemu-system-x86_64 \
  	-drive format=raw,file=./disk.img
  rm ./disk.img
    EOF
  '';

  installPhase = ''
    mkdir -p $out/bin
    mv run $out/bin
    chmod +x $out/bin/run
  '';

  meta = with lib; {
    description = "VM Runner";
    licence = licences.bsd3;
    platforms = platforms.linux;
    mainProgram = "run";
  };
})
