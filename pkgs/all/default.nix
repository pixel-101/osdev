{ lib
, stdenv
, bios
}:

stdenv.mkDerivation (finalAttrs: {
  name = "vm-all";
  src = ./.;

  buildInputs = [
    bios
  ];

  buildPhase = ''
    cat ${bios}/share/boot.bin > full.img
  '';
  installPhase = ''
    mkdir -p $out/share
    mv full.img $out/share
  '';

  meta = with lib; {
    description = "Full binary disk";
    licence = licences.bsd3;
    platforms = platforms.linux;
  };
})
