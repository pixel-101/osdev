{ callPackage, lib, pkgs, selfPkg, ... }:
{
  run = callPackage ./run { all = selfPkg.all; };
  all = callPackage ./all { bios = selfPkg.bios; };
  bios = callPackage ./bios {};
}
